
#Tells the narrative
print("It is the year 100 after the great cataclysm. Your Clan the 'Tuath Arth' lives deep in the northern forests of Cimmeria.")

#Asks the Player for Input
name			= input("What is your Name? ")
status			= input("Your serve your Clan as: ")
	
age 			= input("How old are you? ")
hair			= input("How is your hair? (e.g long and brown, short, curly and decorated with golden rings) ")
beard			= input("How is your beard? ")
stature			= input("How is your stature? (e.g masculine, thin, weak, fat) ")
fighting_skill	= input("What is your fighting skill? (novice, good, seasoned, very skilled, veteran) ")
personality1	= input("What kind of Person are you? (e.g stubborn, intelligent, brave) ")
personality2	= input("Another Character trait: ")
personality3	= input("and one last Character trait: ")

#Saves a generic description of the player's Character
description		= name + " is a " + status + ". He has seen " + age + " winter. His hair is " + hair + \
". His beard is " + beard + ". He seems " + stature + ". He is known as a " + fighting_skill + " fighter." +  \
" He is " + personality1 + ", " + personality2 + " and " + personality3

#Player can choose wether to get a description of their character or exit the program
print("Type in: '1' to get a description of your current Character")
print("Type in: '2' to end the program")

answer = input("What do you want to do? ")

if answer == "1":
	print(description)

if answer == "2":
	exit()



import random
#Defining a Character Class for later use
class Character:
#Initialization Function : Attributes : Calculations
	#This function is called every time an instance of this class is created
		#(17,17)self.cons is a value determined by dividing the sum of Strength and Agility by 2.5self.
		#(21,21)self.life is a value determined by adding the self.cons value to the base Lifepoints of 10
		#(24,24)self.reac is a value wich depends on the weapon the character is wielding.
		#try: except:-constructions are build to help the program avoid quitting. They are catching every Error but the Error message is tailored to ValueErrors
	def __init__(self):
		self.name = input("What is your Name? : ")
		self.stre = input("How strong are you? : ")
		self.agil = input("How agile are you? : ")
		self.dext = input("How is your Dexterity? : ")
		self.perc = input("How is your Perception? : ")
		self.cons = (int(self.stre) + int(self.agil))/2.5
		self.life = 10 + int(self.cons)
		self.reac = 0
		print("What kind of weapon do you use?")
		print("\n'1': Dagger\n'2': Sword\n'3': Hammer\n'4': Axe\n'5': Two-handed Sword\n'6': Bastard Sword\n'7': Bow\n'8': Throwing Weapon\n")
		self.weap = input("Your Answer? : ")
		if self.weap == "1":
			self.dmg = 4
			self.reac -= 2
		elif self.weap == "2":
			self.dmg = 5
			self.reac -= 4
		elif self.weap == "3":
			self.dmg = 3
			self.reac -= 5
		elif self.weap == "4":
			self.dmg = 4
			self.reac -= 4
		elif self.weap == "5":
			self.dmg = 6
			self.reac -= 8
		elif self.weap == "6":
			self.dmg = 5
			self.reac -= 5
		elif self.weap == "7":
			self.dmg = 5
			self.reac -= 5
		elif self.weap == "8":
			self.dmg = 5
			self.reac -= 5
		else:
			print("Invalid Input!")
		self.fight_style = input("How are you fighting? ('1': agile, '2': moderate, '3': aggressive) : ")
		if self.fight_style == "1":
			self.fight_skill = (int(self.agil) + int(self.dext) + int(self.perc))/5
		if self.fight_style == "2":
			self.fight_skill = (int(self.stre) + int(self.agil) + int(self.perc))/5
		if self.fight_style == "3":
			self.fight_skill = (int(self.stre) + int(self.stre) + int(self.perc))/5
		self.armor = input("What Armor do you use? ('1': light, '2': leather/chainmail, '3': heavy) : ")
		if self.armor == "1":
			self.life += 1
		if self.armor == "2":
			self.life += 4
			self.reac -= 4
		if self.armor == "3":
			self.life += 8
			self.reac -= 6

	def show_profile(self):
		print("++++++++++++++++++++++++++++++++++++++++++++++++++")
		print("Name: " + self.name)
		print("Strength: " + self.stre)
		print("Agility: " + self.agil)
		print("Dexterity: " + self.dext)
		print("Perception: " + self.perc)
		print("Constitution: " + str(self.cons))
		print("Lifepoints: " + str(self.life))
		if self.fight_style == "1":
			print("Fighting Style: agile")
		if self.fight_style == "2":
			print("Fighting Style: moderate")
		if self.fight_style == "3":
			print("Fighting Style: aggressive")
		print("Fighting Skill: " + str(self.fight_skill))
		print("Weapon Damage: " + str(self.dmg))
		if self.weap == "1":
			print("Weapon: Dagger")
		if self.weap == "2":
			print("Weapon: Sword")
		if self.weap == "3":
			print("Weapon: Hammer")
		if self.weap == "4":
			print("Weapon: Axe")
		if self.weap == "5":
			print("Weapon: two-handed Sword")
		if self.weap == "6":
			print("Weapon: Bastard Sword")
		if self.weap == "7":
			print("Weapon: Bow")
		if self.weap == "8":
			print("Weapon: Throwing Weapon")
		print("Reaction: " + str(self.reac))
		if self.armor == "1":
			print("Armor: light")
		if self.armor == "2":
			print("Armor: leather/chainmail")
		if self.armor == "3":
			print("Armor: heavy")
		print("++++++++++++++++++++++++++++++++++++++++++++++++++")
		return

#1st Player Character Creation
while True:
	invalid_input = False
	print(" ")
	print("You have 30 Points to spend in Total.")
	print(" ")
	print("Character Creation: Character One")
	print(" ")
	try:
		charone = Character()
		charone.show_profile()
	except:
		invalid_input = True
	if invalid_input == False:
		answer = input("Do you like your Character? ('1': Yes /'2': No) : ")
		print(" ")
	if invalid_input == True:
		answer = input("We had some issues to create your Character. Please ReRoll your Character and read the Instructions carefully\nInput 'Any Key' to ReRoll your Character")
		print(" ")
		print("Character Creation: Character One")
		print(" ")
		charone = Character()
		charone.show_profile()
		while True:														#here and gives 2 Options
			print("Input '1': Show me my Character.")					#First: Show again all Attributes of my Character
			print("Input '2': Continue with Character Two.")			#Second: End my Character Creation and continue with Player Two
			print(" ")
			answer = input("Input: ")									#Waits for the actual Input of the Player
			print(" ")
			if answer == "1":											#Prints all Attributes of Character One on Screen
				charone.show_profile()									#Displays all Character Attributes
				continue 												#Jumps back to the above "Menu"
			if answer == "2":											#Ends this Screen and continues
				break
		break
	if answer == "2":
		continue

	if answer == "1":
		while True:														#here and gives 2 Options
			print("Input '1': Show me my Character.")					#First: Show again all Attributes of my Character
			print("Input '2': Continue with Character Two.")			#Second: End my Character Creation and continue with Player Two
			print(" ")
			answer = input("Input: ")											#Waits for the actual Input of the Player
			print(" ")
			if answer == "1":											#Prints all Attributes of Character One on Screen
				charone.show_profile()									#Displays all Character Attributes
				continue 												#Jumps back to the above "Menu"
			if answer == "2":											#Ends this Screen and continues
				break
		break
	if answer == "2":
		continue

#2nd Player Character Creation
while True:
	print(" ")
	print("Character Creation: Character Two")
	print("")
	chartwo = Character()												#Intantiates a Class. Initializes it's _init() and stores the data in the variable charone
	chartwo.show_profile()
	answer = input("Do you like your Character? ('1': Yes /'2': No) : ")#Waits for Input wether the Character is okay or not
	print(" ")
	if answer == "1":													#If the Answer is Yes the program moves
		while True:														#here and gives 2 Options
			print(" ")													#
			print("Input '1': Show me my Character.")					#First: Show again all Attributes of my Character
			print("Input '2': End Character Creation.")					#Second: End my Character Creation and continue with Player Two
			print(" ")
			answer = input("Input: ")									#Waits for the actual Input of the Player
			if answer == "1":											#Prints all Attributes of Character One on Screen
				chartwo.show_profile()
				continue 												#Jumps back to the above "Menu"
			if answer == "2":											#Ends this Screen and continues
				break
		break
	if answer == "2":
		continue

#3rd Menu: Character Summary
while True:
	print(" ")
	print("Input '1': Show " + charone.name)
	print("Input '2': Show " + chartwo.name)
	print("Input '3': Begin Fight")
	print("Input '4': Quit")
	print(" ")
	answer = input("Input: ")
	if answer == "1":
		charone.show_profile()
		continue
	if answer == "2":
		chartwo.show_profile()
		continue
	if answer == "3":
		break
	if answer == "4":
		quit()

#Fighting Simulation
#while True:
#Initiative Check based of Agility Attribute
#li = low initiative hi = high initiative
#	if charone.agil < chartwo.agil:
#		hi = charone
#		li = chartwo
#		print("Initiative: " + f.name )
#	if charone.agil > chartwo.agil:
#	 	hi = chartwo
#	 	li = charone
#	 	print("initiative: " + f.name)
#	break
#Fight
#	while True:
#		d20 = random.randint(1,21)
#		d6 = random.randint(1,7)
#		hi_d20 = d20
#		li_d20 = d6
#		hi_skill = hi_d20 + int(hi.fight_skill)
#		li_skill = li_d20 + int(li.fight_skill)
#
#		if hi_skill < li_skill:
#			dmg = d6 + int(hi.dmg)
#			life = li.life - dmg
#			print(hi.name + " lands a brutal strike!")
#			print(li.name + " is down to " + life + " Lifepoints" )
#		break

#(247) Starts an Attack from Character One
while True:
	#while True:
	#	d20 = random.randint(1,21)
	#	d6 = random.randint(1,7)
	#	break
	#charone_life = charone.life
	#chartwo_life = chartwo.life
	print("Input '1': " + charone.name + " attacks")
	print("Input '2': " + chartwo.name + " attacks")
	print("Input '3': End Game")
	answer = input("Input: ")

	if answer == "1":
		attacker_skill = int(charone.fight_skill) + random.randint(1,21) + int(charone.reac)
		defender_skill = int(chartwo.fight_skill) + random.randint(1,21) + int(chartwo.reac)

		if attacker_skill > defender_skill:
			dmg = random.randint(1,7) + int(charone.dmg)
			print("dmg: " + str(dmg)) #DEBUGGING DELETE WHEN FINISHED
			chartwo.life -= dmg
			print("The Attack of " + charone.name + " hits " + chartwo.name)
			print(chartwo.name + " has " + str(chartwo.life) + " Lifepoints left")
		else:
			print("The Attack of " + charone.name + " failes!")

	if answer == "2":
		attacker_skill = int(chartwo.fight_skill) + random.randint(1,21) + int(chartwo.reac)
		defender_skill = int(charone.fight_skill) + random.randint(1,21) + int(charone.reac)

		if attacker_skill > defender_skill:
			dmg = random.randint(1,7) + int(chartwo.dmg)
			print("dmg: " + str(dmg)) #DEBUGGING DELETE WHEN FINISHED
			charone.life -= dmg
			print("The Attack of " + chartwo.name + " hits " + charone.name )
			print(charone.name + " has " + str(charone.life) + " Lifepoints left")
		else:
			print("The Attack of " + chartwo.name + " failes!")

	if answer == "3":
		quit()
